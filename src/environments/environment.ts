// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC9ZJB7Pqyb4xdF_PZ1tk07sADxFzyicAI',
    authDomain: 'library-project-747fd.firebaseapp.com',
    projectId: 'library-project-747fd',
    storageBucket: 'library-project-747fd.appspot.com',
    messagingSenderId: '137601903170',
    appId: '1:137601903170:web:b227bf64cf9bd010699bdb',
    measurementId: 'G-M5GDQWREMF',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
