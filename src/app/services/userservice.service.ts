import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UserModel } from '../_models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserserviceService {
  users: [] | any;

  constructor(
    private firestore: AngularFirestore,
    private httpClient: HttpClient
  ) {}

  getAllUsers(): Observable<UserModel[]> {
    return this.firestore.collection('users').valueChanges() as Observable<
      UserModel[]
    >;
  }

  getMockUsers() {
    return this.httpClient.get('../assets/');
  }
}
