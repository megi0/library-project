import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BookModel } from '../_models/book.model';

@Injectable({
  providedIn: 'root',
})
export class BookservicesService {
  constructor(private fs: AngularFirestore) {}

  getAllBooks(): Observable<BookModel[]> {
    return this.fs.collection('books').valueChanges() as Observable<
      BookModel[]
    >;
  }
}
