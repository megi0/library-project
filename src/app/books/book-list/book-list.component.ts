import { Component, Input, OnInit } from '@angular/core';
import { BookservicesService } from 'src/app/services/bookservices.service';
import { BookModel } from 'src/app/_models/book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
})
export class BookListComponent implements OnInit {
  public books: BookModel[] = [];

  ngOnInit(): void {}

  constructor(private bs: BookservicesService) {
    this.bs.getAllBooks().subscribe((response: BookModel[]) => {
      this.books = response;
    });
  }
}
