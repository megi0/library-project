import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  ReactiveFormsModule,
} from '@angular/forms';
import { BookModel } from 'src/app/_models/book.model';
import { BookservicesService } from 'src/app/services/bookservices.service';
import { canComponentLeave } from '../../guards/unsaved-changes.guard';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css'],
})
export class CreateBookComponent implements OnInit, canComponentLeave {
  allBooks: BookModel[] = [];
  public createBookForm = this.formBuilder.group({
    title: [''],
    author: [''],
    isbn: [''],
    pages: [''],
    genre: [''],
  });

  constructor(
    private formBuilder: FormBuilder,
    private bService: BookservicesService
  ) {}

  ngOnInit(): void {
    this.bService.getAllBooks().subscribe((response: BookModel[]) => {
      //this.allBooks = response;
      console.log(response);
    });
  }

  setTitle() {
    //this.createBookForm.get('title').setValue('Lorem Ipsum');
  }

  showTitle() {
    console.log(this.createBookForm.get('title'));
  }

  canLeave(): boolean {
    return true;
  }
}
