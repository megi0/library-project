import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateBookComponent } from './create-book/create-book.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookComponent } from './book/book.component';
import { BookListComponent } from './book-list/book-list.component';

@NgModule({
  declarations: [CreateBookComponent, BookComponent, BookListComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [CreateBookComponent],
})
export class BookModule {}
