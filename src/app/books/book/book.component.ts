import { Component, Input, OnInit } from '@angular/core';
import { BookModel } from 'src/app/_models/book.model';
import { BookservicesService } from '../../services/bookservices.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {
  @Input() book: BookModel | undefined;
  public edit = false;

  ngOnInit(): void {}
}
