export interface UserModel {
  email: string;
  password: string;
  name: string;
  birthDate?: Date;
  gender?: string;
  username: string;
}
