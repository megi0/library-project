import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegexValidators } from 'src/app/validators/regexValidators.validator';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  signupGroup = this.signupFormBuilder.group({
    name: ['', Validators.required],
    surname: [''],
    email: ['', RegexValidators.email],
    password: ['', RegexValidators.password],
    gender: [''],
  });

  constructor(private signupFormBuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    console.log(this.signupGroup);
    this.router.navigate(['/home']);
  }

  genders: String[] = ['Female', 'Male', 'Non-binary'];
}
