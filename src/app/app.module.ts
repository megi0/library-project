import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookModule } from './books/book.module';
import { LoginModule } from './login/login.module';
import { SignupModule } from './signup/signup.module';
import { AddAuthorComponent } from './author/add-author/add-author.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './services/auth-guard.service';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { UserserviceService } from './services/userservice.service';
import { HttpClientModule } from '@angular/common/http';
import { BookservicesService } from './services/bookservices.service';
import { AuthenticationService } from './services/authentication.service';
@NgModule({
  declarations: [
    AppComponent,
    AddAuthorComponent,
    HeaderComponent,
    HomeComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    BookModule,
    ReactiveFormsModule,
    FormsModule,
    SignupModule,
    HttpClientModule,
  ],
  providers: [
    AuthGuardService,
    UserserviceService,
    BookservicesService,
    AuthenticationService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
