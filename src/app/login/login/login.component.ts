import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/_models/user.model';
import { UserserviceService } from '../../services/userservice.service';
import { AuthenticationService } from '../../services/authentication.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  users: any;
  allUsers: UserModel[] = [];
  password: string = '';
  email: string = '';

  constructor(
    private router: Router,
    private fs: AngularFirestore,
    private us: UserserviceService,
    private au: AuthenticationService
  ) {
    this.users = this.fs.collection('users').valueChanges();
    console.log(this.users);
  }

  ngOnInit(): void {
    this.us.getAllUsers().subscribe((response: UserModel[]) => {
      this.allUsers = response;
      console.log(response);
    });
  }

  onSubmit() {
    let foundUser: boolean = false;
    this.allUsers.forEach((user: UserModel) => {
      if (user.email === this.email && user.password === this.password) {
        foundUser = true;
        this.au.setLoggedIn(true);
        this.router.navigate(['/home']);
      }
    });
    if (!foundUser) console.error('User does not exist');
  }
}
