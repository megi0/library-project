import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css'],
})
export class AddAuthorComponent implements OnInit {
  public authorFormGroup = this.authorFormBuilder.group({
    name: [''],
    surname: [''],
    booksNumber: [''],
    titles: new FormArray([this.createElement()]),
  });

  constructor(private authorFormBuilder: FormBuilder) {}

  ngOnInit(): void {}

  get titles() {
    return this.authorFormGroup.controls['titles'] as FormArray;
  }

  addAuthor() {
    this.titles.push(this.createElement());
  }

  createElement() {
    return new FormControl('');
  }

  onSubmit() {
    console.log(this.authorFormGroup.value);
  }
}
