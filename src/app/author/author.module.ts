import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAuthorComponent } from './add-author/add-author.component';

@NgModule({
  declarations: [AddAuthorComponent],
  imports: [CommonModule],
  exports: [AddAuthorComponent],
})
export class AuthorModule {}
