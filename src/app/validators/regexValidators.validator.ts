import { Directive } from '@angular/core';
import {
  ValidatorFn,
  AbstractControl,
  ControlContainer,
  NG_VALIDATORS,
} from '@angular/forms';

export class RegexValidators {
  static emailRegex: RegExp = /[^@]+@[^\.]+\..+/;
  static passwordRegex: RegExp = /^[a-zA-Z0-9]+$/;

  static email(control: AbstractControl): any | null {
    return RegexValidators.regexDependentValidation(RegexValidators.emailRegex)(
      control
    );
  }

  static password(control: AbstractControl): any | null {
    return RegexValidators.regexDependentValidation(
      RegexValidators.passwordRegex
    )(control);
  }

  static regexDependentValidation(regex: RegExp): ValidatorFn {
    console.log(regex);
    return (control: AbstractControl): { [key: string]: any } | null =>
      control.value.match(regex) ? null : { doesNotMatch: control.value };
  }
}
