import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAuthorComponent } from './author/add-author/add-author.component';
import { CreateBookComponent } from './books/create-book/create-book.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SignupComponent } from './signup/signup/signup.component';
import { BookListComponent } from './books/book-list/book-list.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: SignupComponent },
  {
    path: 'author',
    component: AddAuthorComponent,
  },
  { path: 'books', component: BookListComponent },
  { path: 'home', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
